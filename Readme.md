## Deployments

Tudo começa com a configuração do deployment (``k8s/deployment.yaml``), onde são configuradas as informações pertinentes ao POD: especificações do container, limites de recursos, health checks etc. Normalmente um deployment por container.

## Metrics server

Para possibilitar a criação de auto scaling e monitoramento de recursos, é necessário instalar o metrics-server no cluster (https://github.com/kubernetes-sigs/metrics-server). Para instalar em um cluster sem tls, foi necessário customizar a instalação (``k8s/metrics-server.yaml``) 

## Horizontal Pod Autoscaler

Tendo instalado o metrics-server, é possível configurar as regras de escalonamento dos pods. O HPA é sempre vinculado a um deployment ou statefulset.

Nele são definidas os limites de min e max de replicas para o deployment e as métricas que devem ser monitoradas nas políticas de auto scaling.

O arquivo de configuração correspondente é ``k8s/hpa.yml``.

## Services

Os services são responsáveis por expor os pods e possibilitar o seu acesso. É necessário configurar o tipo (normalmente ClusterIP) e o apontamento da porta exposta no service para a porta do container. Os services também são responsáveis por realizar o load balance entre os pods. O que determina os pods que serão vinculados ao service é a configuração ``selector``. O link é feito através dos labels. Um service por deployment.

Configuração no arquivo ``k8s/service.yml``.

## Service Accounts ‼️‼️‼️

É muito importante não utilizar a service account default, pois ela pode ser explorada no caso de vazamento de acessos, possibilitando acesso a todas as apis do kubernetes de qualquer pod.

Por padrão, todos os pods criados são vinculados a service account padrão, que dá acesso a todas as operações e serviços do kubernetes. Se algum pod for invadido, o invasor pode utilizar o token armazenado no pod para gerenciar o cluster inteiro‼️‼️

Para solucionar este problema, é possível criar novos services accounts com roles específicas. O arquivo de configuração ``k8s/security.yaml`` exemplifica a criação de uma service account, uma role e uma role binding que dá permissão apenas para leitura dos recursos do cluster.

A service account deve ser referenciada na criação do deployment, conforme exemplificado no arquivo ``k8s/namespaces/deployment.yaml``. 

## Ingress

O Ingress atua como uma espécie de api gateway e realiza o roteamento das requisições para os services, de acordo com a configuração. O tipo mais comum é o ingress-nginx (https://kubernetes.github.io/ingress-nginx/deploy/#using-helm) e ele deve ser instalado no cluster e posteriormente configurado (ingress.yaml).

É no ingress onde as configurações de tls são feitas.

## Cerficado TLS

Para realizar a instalação do tls é necessário instalar o cert-manager (https://cert-manager.io/docs/) no cluster.

Feito isso, é necessário configurar o ``k8s/cluster-issuer.yaml``, que contém as informações do issuer do certificado.

Depois, é necessário incluir a configuração de tls no ingress (que serve como porta de entrada para os services).

## Namespaces

São separações lógicas dos recursos e permissões. Ideal criar um namespace por projeto é recomendado. Os namespaces são definidos no objeto ``metadata`` nos objetos yaml.

``kubectl create ns nome-namespace``

Para provisionar os recursos no namespace via command line:

``kubectl apply -f k8s/ -n=nome-namespace``

## Contextos

Para melhorar a separação dos recursos e evitar problemas como deployments executados em namespaces incorretos, é possível criar um contexto para um namespace.

``kubectl config set-context dev --namespace=dev --cluster=kind-fullcycle --user=kind-fullcycle``

``kubectl config set-context prod --namespace=prod --cluster=kind-fullcycle --user=kind-fullcycle``

Para utilizar o contexto:

``kubectl config use-context dev``

Assim, todas as operações serão realizadas no namespace ``dev``.

Para verificar o contexto selecionado:

``kubectl config current-context``


## Comandos

### Lista os recursos da API do Kubernetes (boa para pegar a apiVersion)

``kubectl api-resources``

### Lista os deployments

``kubectl get deployments``

### Lista as replicasets

``kubectl get replicasets``

### Lista os pods

``kubectl get pod``

### Delete algum recurso

``kubectl delete [tipo do recurso (pod, replicaset, deployment)] [nome do recurso]``

### Aplica uma configuração (neste caso k8s/deployment.yaml )

``kubectl apply -f k8s/deployment.yaml``

### Exibe o histórico das revisões de um deployment

``kubectl rollout history deployment goserver``

### Faz o undo de algum deployment

``kubectl rollout undo deployment goserver``

### Faz o undo de algum deployment para alguma revision específica

``kubectl rollout undo deployment goserver --to-revision=[numero revisao]``

### Consulta informações detalhadas de algum recurso

``kubectl describe [tipo de recurso] [nome de recurso]``

### Faz o port forward para acessar algum recurso (pod ou service)

``kubectl port-forward svc/goserver-service 8000:80``

### Cria um proxy para acessa a API do kubernetes

``kubectl proxy --port=8080``

### Executa um comando no POD 🚀👀

``kubectl exec -it goserver-675dc7786f-cxs4t -- bash``

### Ver logs de um pod ‼️‼️

``kubectl logs [nome pod]``

### Aplica a configuração e fica monitorando os pods

``kubectl apply -f k8s/deployment.yaml && watch -n1 kubectl get pods``

### Roda um pod para executar algum teste e já remove ao finalizar (—rm)

``kubectl run -it fortio --rm --image=fortio/fortio -- load -qps 800 -t 120s -c 70 "http://goserver-service/healthz"``

### Escalar um recurso manualmente

``kubectl scale deployment goserver --replicas=5``

### Listar pods de um namespace

``kubectl get po -n [nome-namespace]``

### Lista os certificados

``kubectl get certificates``
